<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/10
 * Time: 下午6:50
 */

namespace Once\Container;


use Illuminate\Support\Facades\App;
use Laravel\Lumen\Application;
use Once\Metas\PropertyMeta;
use Once\Utils\AnnotationsVisitor;
use Once\Utils\Validation;
use Once\Utils\Validator;
use Once\Utils\Verify;

class EntityContainer extends ClassAnnotations
{
    public function __construct($className)
    {
        $this->className = $className;
        //TODO 缓存
        $refl = new \ReflectionClass($className);

        $docFactory  = AnnotationsVisitor::createDocBlockFactory();
        if($refl->getDocComment()){
            $docblock = $docFactory->create($refl->getDocComment());
            $this->doc = $docblock->getSummary();
        }


        $properties = $refl->getProperties(\ReflectionProperty::IS_PUBLIC);
        $default = $refl->getDefaultProperties();
        $this->fileName = $refl->getFileName();
        foreach ($properties as $i){
            $isOption = \Once\Utils\Arr::has($default, $i->getName()) && $default[$i->getName()] !==null;
            if($i->getDocComment()) {
                $docblock = $docFactory->create($i->getDocComment());
                $doc = $docblock->getSummary();//TODO * 去掉已经解析出的@
            }else{
                $doc = '';
            }

            $this->properties[$i->getName()] = new PropertyMeta(
                $i->getName(),
                null,
                $isOption,
                $isOption?$default[$i->getName()]:null,
                null,
                $doc);
        }
    }

    public function getProperty($target){
        \Once\Utils\Arr::has($this->properties, $target) or Verify::fail("property $target not exist");
        return $this->properties[$target];
    }

    /**
     * @return PropertyMeta[]
     */
    public function getProperties(){
        return $this->properties;
    }
    public function getDoc(){
        return $this->doc;
    }

    /**
     * @param Application $app
     * @param array $params
     */
    public function make(Application $app, array $params){
        if($params === null){
            return $params;
        }
        is_array($params) or Verify::fail(
            new \InvalidArgumentException("array is require by param 2")
        );
        $vld = new Validator($app);
        $input = [];
        foreach ($this->getProperties() as $property){
            if(\Once\Utils\Arr::has($params, $property->name)){
                $value = $params[$property->name];
                //TODO 对象嵌套
                $vld->addRule($property->name, $property->type, $property->validation);
                $input[$property->name] = $value;
            }else{
                $property->isOptional or  Verify::fail(new \InvalidArgumentException("property {$property->name} is required by {$this->className}"));
                $input[$property->name] = $property->default;
            }
        }
        $input = $vld->validate($input);
        $output = $app->make($this->className);
        foreach ($input as $k=>$v){
            $output->{$k}=$v;
        }
        return $output;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }
    /**
     * @var PropertyMeta[]
     */
    private $properties=[];

    /**
     * @var string
     */
    private $className;

    /**
     * @var string
     */
    private $doc='';

    /**
     * @var string
     */
    private $fileName;
}