<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/16
 * Time: 上午10:13
 */


if (! function_exists('model')) {
    /**
     * @param string $entityClass
     * @return \Once\Orm\Model
     */
    function model($entityClass, $exist = false)
    {
        return \Once\Orm\Model::createByEntity($entityClass, $exist);
        //return new \Once\Orm\Model($entityClass);
    }
}