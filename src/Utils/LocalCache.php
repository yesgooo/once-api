<?php

namespace Once\Utils;

/**
 * 
 * @author caoym
 */
class LocalCache extends CheckableCache
{
    public function __construct($tag='')
    {
        if (!function_exists('apc_fetch') || !function_exists('apc_store') || !function_exists('apc_delete')) {
            parent::__construct(new FileCache(), $tag);
        }else{
            parent::__construct(new ApcCache(), $tag);
        }
    }
}
