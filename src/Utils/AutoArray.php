<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/13
 * Time: 下午6:08
 */

namespace Once\Utils;


/**
 * Class AutoArray
 * @package Once\Utils
 * 元素被访问时自动创建
 */
class AutoArray extends \ArrayObject
{
    public function offsetExists($index) {
        if(!parent::offsetExists($index)){
            $this->offsetSet($index, null);
            return true;
        }
        return true;
    }
}