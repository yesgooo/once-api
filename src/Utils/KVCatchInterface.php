<?php

namespace Once\Utils;

interface KVCatchInterface
{
    /**
     * 设置key
     * @param string $key
     * @param mixed $var
     * @param int $ttl
     * @return boolean
     */
    public function set($key, $var, $ttl);
    /**
     * 删除key
     * @param string $key
     * @return boolean
     */
    public function del($key);
    /**
     * get key
     * @param string $key
     * @param boolean $succeeded
     * @return mixed
     */
    public function get($key, &$succeeded);
    
}