<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/9/27
 * Time: 下午3:26
 */

namespace Once\Utils;

/**
 * Class Verify
 * @package Once\Utils
 */
class Verify
{
    /**
     * @param string|\Exception $err
     * @throws \Exception
     */
    static public function fail($err=''){
        if($err instanceof \Exception){
            throw $err;
        }else{
            throw new \Exception($err);
        }
    }

    /**
     * @param mixed $var
     * @param \Exception|string $msg
     * @return mixed
     * @throws \Exception
     */
    static public function isTrue($var, $msg=''){
        if (!$var) {
            if($msg === null || is_string($msg)){
                Logger::warning($msg);
                throw new \Exception($msg);
            }else{
                Logger::warning($msg->__toString());
                throw $msg;
            }
        } else {
            return $var;
        }
    }

}