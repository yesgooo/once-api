<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/13
 * Time: 下午11:13
 */

namespace Once\Annotations\Controller;

namespace Once\Annotations\Controller;
use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;
use Once\Exceptions\AnnotationSyntaxExceptions;
use Once\Utils\TypeHint;
use Once\Utils\Verify;

class AnnThrows extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container){
        $this->container = $container;
    }

    /**
     * @param $target
     * @param $name
     * @param $value
     * @return bool
     */
    protected function handleMethod($target, $name, $value)
    {
        $route = $this->container
            ->getRoute($target);

        if(!$route){
            return false;
        }
        $params = $this->getParams($value, 2);
        count($params)>0 or Verify::fail(new AnnotationSyntaxExceptions("something wrong with @throw $value"));

        $type = TypeHint::normalize($params[0]); // TODO 缺少类型时忽略错误
        $doc = count($params)>0?$params[1]:'';

        $route->getActionInvoker()
            ->addExceptions($type, $doc);
        return true;
    }

    /**
     * @var ControllerContainer
     */
    private $container;
}