<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/17
 * Time: 下午1:09
 */

namespace Once\Annotations\Controller;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;
use Once\Exceptions\AnnotationSyntaxExceptions;
use Once\Utils\TypeHint;

/**
 * Class AnnMid
 * @package Once\Annotations\Controller
 * 声明路由使用的中间件
 */
class AnnMid extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container){
        $this->container = $container;
    }
    protected function handleMethod($target, $name, $value)
    {
        $params = $this->getParams($value, 2);
        count($params)>=1 or Verify::fail(new AnnotationSyntaxExceptions("\"@o-mid <middlewares>\" miss params for $target"));
        if($route = $this->container->getRoute($target)){
            $route->setMiddlewares(TypeHint::normalize($params[0], $this->container->getClassName()));
            return true;
        }
        return false;
    }

    /**
     * @var ControllerContainer
     */
    private $container;
}