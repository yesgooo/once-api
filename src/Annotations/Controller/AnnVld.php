<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/10
 * Time: 下午6:48
 */

namespace Once\Annotations\Controller;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;

class AnnVld extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container, BaseAnnotationHandler $parent=null){
        $this->container = $container;
        $this->parent = $parent;
    }

    /**
     * @param $target
     * @param $name
     * @param $value
     * @return bool
     */
    protected function handleMethod($target, $name, $value)
    {
        $route = $this->container->getRoute($target);
        if(!$route) {
            return false;
        }
        if ($this->parent ==null){
            \Once\Utils\Logger::warnging("@$name should be used with a parent annotation");
            return false;
        }
        $params = $this->getParams($value, 2);
        count($params)>0 or Verify::fail(new AnnotationSyntaxExceptions("something wrong with @o-vld $value"));

        $doc = count($params)>0?$params[1]:'';

        if($this->parent instanceof AnnParam){
            $paramMeta = $route->getActionInvoker()->getParamsBuilder()->getParam($this->parent->paramName);
            $paramMeta->validation = $params[0];
            $paramMeta->doc = $doc;
            return true;
        }
        \Once\Utils\Logger::warnging("@o-vld not work with parent ".get_class($this->parent));
        return false;
    }

    /**
     * @var ControllerContainer
     */
    private $container;

    /**
     * @var AnnParam
     */
    private $parent;
}