<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/10/9
 * Time: 下午8:30
 */

namespace Once\Annotations\Controller;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;

/**
 * Class Path
 * @package Once\Annotations
 * 用于注释controller 类对应的路由前缀, 如"@o-path /users/"
 */
class AnnPath extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container){
        $this->container = $container;
    }
    protected function handleClass($target, $name, $value)
    {
        $params = $this->getParams($value,2) + [''];
        $this->container->setPathPrefix($params[0]);
        return true;
    }

    /**
     * @var ControllerContainer
     */
    private $container;

}