<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/10/9
 * Time: 下午10:49
 */

namespace Once\Annotations\Controller;

use FastRoute\RouteParser\Std;
use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;
use Once\Container\Route;
use Once\Exceptions\AnnotationSyntaxExceptions;
use Once\Container\ActionInvoker;
use Once\Metas\ParamMeta;
use Once\Container\ParamsBuilder;
use Once\Utils\AnnotationsVisitor;
use Once\Utils\Verify;

/**
 * Class Route
 * @package Once\Annotations
 */
class AnnRoute extends BaseAnnotationHandler
{

    public function __construct(ControllerContainer $container){
        $this->container = $container;
    }
    protected function handleMethod($target, $name, $value)
    {
        $params = $this->getParams($value, 3);
        count($params)>=2 or Verify::fail(new AnnotationSyntaxExceptions("\"$name $value\" miss params for $target"));
        //TODO 错误判断: METHOD不支持, path不规范等
        $httpMethod = strtoupper($params[0]);
        in_array($httpMethod,[
            'GET',
            'POST',
            'PUT',
            'HEAD',
            'PATCH',
            'OPTIONS',
            'DELETE'
        ]) or Verify::fail(new AnnotationSyntaxExceptions("unknown method http $httpMethod in $name $value"));
        //获取方法参数信息
        $refl =  new \ReflectionClass($this->container->getClassName());
        $method = $refl->getMethod($target);
        $methodParams = $method->getParameters();

        $docFactory  = AnnotationsVisitor::createDocBlockFactory();
        $docblock = $docFactory->create($method->getDocComment()?:"");
        //从路由中获取变量, 用于判断参数是来自路由还是请求
        $routeParser = new Std();
        $info = $routeParser->parse($params[1]); //0.4和1.0返回值不同, 不兼容
        $routeParams = [];
        foreach ($info as $i){
            if(is_array($i)){
                $routeParams[$i[0]] = true;
            }
        }
        //设置参数列表
        $paramsMeta = [];
        foreach ($methodParams as $param){
            $paramName = $param->getName();

            $source = "$.request.input.$paramName";//默认情况参数来自input, input为post+get的数据
            if(\Once\Utils\Arr::has($routeParams, $paramName)){ //参数来自路由
                $source = "$.request.route.$paramName";
            }elseif($httpMethod == 'GET'){
                $source = "$.request.query.$paramName"; //GET请求显示指定来自query string
            }

            $paramsMeta[] = new ParamMeta($paramName,
                $source,
                $param->getType(),
                $param->isOptional(),
                $param->isOptional()?$param->getDefaultValue():null,
                $param->isPassedByReference(),
                null,
                ""
            );
        }

        $paramsBuilder = new ParamsBuilder($paramsMeta);

        $route = new Route(
            $httpMethod,
            $params[1],
            null,
            new ActionInvoker($target, $paramsBuilder),
            $docblock->getSummary()
        );

        $this->container->addRoute($target, $route);
        return true;
    }

    /**
     * 获取路径中包含的参数
     * @param $path
     */
    static function getPathParams($path){

    }
    /**
     * @var ControllerContainer
     */
    private $container;
}