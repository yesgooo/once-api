<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/10/9
 * Time: 下午10:10
 */

namespace Once\Annotations;


use Once\Container\ControllerContainer;
use Once\Utils\AnnotationsVisitor;

class BaseAnnotationHandler implements AnnotationHandler
{

    /**
     * @param $type @see AnnotationsVisitor::TYPE_*
     * @param string $target the name of the class or method or property
     * @param string $name tag name
     * @param string $value
     * @return boolean
     */
    public function handle($type, $target, $name, $value)
    {
        if ($type == AnnotationsVisitor::TYPE_CLASS){
            return $this->handleClass($target, $name, $value);
        }elseif ($type == AnnotationsVisitor::TYPE_METHOD){
            return $this->handleMethod($target, $name, $value);
        }elseif ($type == AnnotationsVisitor::TYPE_PROPERTY){
            return $this->handleProperty($target, $name, $value);
        }else{
            return false;
        }
    }

    /**
     * @param string $target
     * @param string $name
     * @param string $value
     * @return bool
     */
    protected function handleClass($target, $name, $value){
        \Once\Utils\Logger::warning("@$name with class not supported");
        return false;
    }

    protected function handleMethod($target, $name, $value){
        \Once\Utils\Logger::warning("@$name with method not supported");
        return false;
    }

    protected function handleProperty($target, $name, $value){
        \Once\Utils\Logger::warning("@$name with property not supported");
        return false;
    }

    /**
     * 将输入分割成指定数量的片段, 分隔符可以是"空格\n\t"
     * @param string $from
     * @param int $limit
     * @return array
     */
    public function getParams($from, $limit=null){
        //TODO ** 支持引号和转意符
        $pos = 0;
        $len = strlen($from);
        $space = false;
        $begin = 0;
        $params = [];
        while($pos < $len){
            if(!$space){
                if(trim($from[$pos]) == ''){
                    $space = true;
                    $params[] = substr($from, $begin, $pos-$begin);
                    $begin = $pos;
                    if($limit != null){
                        if(count($params) >= $limit-1){
                            break;
                        }
                    }
                }
            }else{
                if(trim($from[$pos]) != ''){
                    $space = false;
                    $begin = $pos;
                }
            }
            $pos++;

        }
        if ($begin != $len){
            $lastone = substr($from, $begin, $len-$begin);
            $params[] = ltrim($lastone);
        }
        return $params;
    }

    /**
     * 将输入分割成指定数量的片段, 分隔符可以是"空格\n\t"(临时命名,稳定后直接替换getParams)
     * @param string $from
     * @param int $limit
     * @return array
     */
    private function newGetParams($form, $limit=null)
    {
        $params = [];
        $endParams = "";
        $basePattern = '\\\"|\\\\|[A-Za-z0-9]|[\x{4e00}-\x{9fa5}]|\/';
        $pattern = '/(\"('. $basePattern .'|\s)+\")|('. $basePattern .')+/u';
        preg_match_all($pattern, $form, $match);
        $match = current($match);

        foreach ($match as $k=>$v){
            if($limit == null || $k < $limit){
                $v = str_replace('\\"', '"', $v);
                $v = str_replace('\\\\', '\\', $v);
                $v = trim($v, '"');
                $params[] = $v;
            } else {
                if(!empty($endParams)){$v = " " . $v;}
                $endParams .= $v;
            }
        }

        $params[] = $endParams;

        return $params;
    }

}