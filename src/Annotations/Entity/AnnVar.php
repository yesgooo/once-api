<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/10
 * Time: 下午6:45
 */

namespace Once\Annotations\Entity;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\EntityContainer;
use Once\Utils\TypeHint;

class AnnVar extends BaseAnnotationHandler
{
    /**
     * AnnVar constructor.
     * @param EntityContainer $container
     */
    public function __construct(EntityContainer $container){
        $this->container = $container;
    }

    protected function handleProperty($target, $name, $value){
        $params = $this->getParams($value, 2);
        if(count($params)){
            $type = $params[0];
            //TODO 校验type类型
            $property = $this->container->getProperty($target);
            if($type){
                // TODO 判断$type是否匹配
                $property->type = TypeHint::normalize($type, $this->container->getClassName());
            }
            if(count($params)>1){
                $property->doc = $params[1];
            }

        }
        return false;
    }

    /**
     * @var EntityContainer
     */
    private $container;

    //TODO 支持依赖注入
    //TODO * 支持声明默认值
}