<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/10
 * Time: 下午6:48
 */

namespace Once\Annotations\Entity;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\EntityContainer;

class AnnVld extends BaseAnnotationHandler
{
    /**
     * AnnVld constructor.
     * @param EntityContainer $container
     */
    public function __construct(EntityContainer $container){
        $this->container = $container;
    }

    protected function handleProperty($target, $name, $value){

        $params = $this->getParams($value, 2);
        if(count($params)){
            $property = $this->container->getProperty($target);
            $property->validation = $params[0]; //TODO 校验语法
            //TODO ** 更新属性的注释, 去掉@-vld
        }
        return false;
    }

    /**
     * @var EntityContainer
     */
    private $container;
}