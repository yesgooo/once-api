<?php
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/15
 * Time: 下午10:20
 */

namespace Once\Annotations\Entity;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\EntityContainer;

class AnnTable extends BaseAnnotationHandler
{
    /**
     * AnnVar constructor.
     * @param EntityContainer $container
     */
    public function __construct(EntityContainer $container){
        $this->container = $container;
    }

    protected function handleClass($target, $name, $value){
        $params = $this->getParams($value, 2);
        if(count($params)){
            $this->container->addClassAnnotation($name, [$params[0]]);
            return true;

        }
        return false;
    }

    /**
     * @var EntityContainer
     */
    private $container;
}