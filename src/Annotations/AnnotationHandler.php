<?php

/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/10/9
 * Time: 下午8:23
 */

namespace Once\Annotations;


interface AnnotationHandler{
    /**
     * @param $type @see AnnotationsVisitor::TYPE_*
     * @param string $target the name of the class or method or property
     * @param string $name tag name
     * @param string $value
     * @return boolean
     */
    public function handle($type, $target, $name, $value);


}
