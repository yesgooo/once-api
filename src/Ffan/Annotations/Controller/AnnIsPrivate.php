<?php
/**
 * Created by PhpStorm.
 * User: huanglong
 * Date: 16/12/05
 * Time: 下午11:18
 */

namespace Once\Ffan\Annotations\Controller;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;

class AnnIsPrivate extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container, BaseAnnotationHandler $parent=null){
        $this->container = $container;
        $this->parent = $parent;
    }

    /**
     * @param $target
     * @param $name
     * @param $value
     * @return bool
     */
    protected function handleMethod($target, $name, $value)
    {

        $params = $this->getParams($value, 1);
        if(sizeof($params < 1)){
            \Once\Utils\Logger::warning("params is not a valid value");
        }
        $public = $params[0];
        if($public != 'true' && $public != 'false'){
            \Once\Utils\Logger::warning("$public is not a valid value");
        }
        $this->container->addMethodAnnotation($target, 'ff-public', [$public]);
    }

    /**
     * @var ControllerContainer
     */
    private $container;

    /**
     * @var BaseAnnotationHandler
     */
    private $parent;
}


