<?php
/**
 * Created by PhpStorm.
 * User: huanglong
 * Date: 16/12/05
 * Time: 下午11:18
 */

namespace Once\Ffan\Annotations\Controller;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;

class AnnPrivate extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container, BaseAnnotationHandler $parent=null){
        $this->container = $container;
        $this->parent = $parent;
    }

    /**
     * @param $target
     * @param $name
     * @param $value
     * @return bool
     */
    protected function handleMethod($target, $name, $value)
    {
        $params = $this->getParams($value, 1);

        try{
            if(sizeof($params) < 1){
                throw new \Exception("params size < 1");
            }
            $private = $params[0];
            if($private != 'true' && $private != 'false'){
                throw new \Exception("$private is not a valid value");
            }
            $this->container->addMethodAnnotation($target, 'ff-private', [$private]);
        }catch (\Exception $e){
            \Once\Utils\Logger::warning("{$e->getMessage()}");
        }
    }

    /**
     * @var ControllerContainer
     */
    private $container;

    /**
     * @var BaseAnnotationHandler
     */
    private $parent;
}


