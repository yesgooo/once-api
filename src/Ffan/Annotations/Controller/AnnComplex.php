<?php
/**
 * Created by PhpStorm.
 * User: huanglong
 * Date: 16/12/05
 * Time: 下午11:18
 */

namespace Once\Ffan\Annotations\Controller;

use Once\Annotations\BaseAnnotationHandler;
use Once\Container\ControllerContainer;

class AnnComplex extends BaseAnnotationHandler
{
    public function __construct(ControllerContainer $container, BaseAnnotationHandler $parent=null){
        $this->container = $container;
        $this->parent = $parent;
    }

    /**
     * @param $target
     * @param $name
     * @param $value
     * @return bool
     */
    protected function handleMethod($target, $name, $value)
    {
        $params = $this->getParams($value, 1);

        try{
            if(sizeof($params) < 1){
                throw new \Exception("params size < 1");
            }
            $complex = $params[0];
            if($complex != 'true' && $complex != 'false'){
                throw new \Exception("$complex is not a valid value");
            }
            $this->container->addMethodAnnotation($target, 'ff-complex', [$complex]);
        }catch (\Exception $e){
            \Once\Utils\Logger::warning("{$e->getMessage()}");
        }
    }

    /**
     * @var ControllerContainer
     */
    private $container;

    /**
     * @var BaseAnnotationHandler
     */
    private $parent;
}


