<?php
require __DIR__.'/../vendor/autoload.php';
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/11
 * Time: 下午5:33
 */
class ValidatorTest extends TestCase
{

    /**
     * 测试原始类型
     */
    public function testScalarType(){
        $vld = new \Once\Utils\Validator($this->app);
        $vld->addRule('a', 'int', 'int|min:1');
        $this->assertSame(
            $vld->validate(['a'=>123]),
            ['a'=>123]
        );
    }

    //TODO 测试对象校验
    //TODO 测试嵌套对象校验
    //TODO 支持基本类型的别名
}