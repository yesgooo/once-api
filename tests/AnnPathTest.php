<?php
require __DIR__.'/../vendor/autoload.php';
/**
 * Class AnnPathMock
 * @path /the-valid-path/
 */
class AnnPathMock{
    
}
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/8
 * Time: 下午2:26
 */
class AnnPathTest extends TestCase
{

    public function testValidPath(){
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, AnnPathMock::class);
        $this->assertEquals($container->getPathPrefix(), "/the-valid-path/");
        $this->assertEquals($container->getDoc(), "Class AnnPathMock");
    }
    //TODO 无效的@path检查
}