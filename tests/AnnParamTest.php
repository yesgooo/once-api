<?php
require __DIR__.'/../vendor/autoload.php';

use \Once\Annotations\Controller\AnnParam;


/**
 * Class ValidParam
 */
class ValidParam{

    /**
     * @o-route GET /
     */
    public function funWithoutParams0($param0, $param1=1){
        //
    }
    /**
     * @o-route GET /
     * @param string $param0
     */
    public function funWithoutParams1($param0, $param1=1){
        //
    }

    /**
     * @o-route GET /
     * @param  string   $param0 doc
     */
    public function funWithoutParams2($param0){
        //
    }
    /**
     * @o-route GET /
     * @param $param0 doc
     */
    public function funWithoutParams3($param0){
        //
    }
    /**
     * @o-route GET /
     * @param $param0
     */
    public function funWithoutParams4($param0){
        //
    }
    /**
     * @o-route GET /
     * @param $param0
     */
    public function funWithoutParams5(&$param0){
        //
    }

    /**
     * @o-route GET /funWithoutParams6
     * @param AnnParam $param0
     */
    public function funWithoutParams6($param0){
        //
    }
}

class InvalidParam0{
    /**
     * @o-route GET /
     * @param
     */
    public function test($param0, $param1=1){
        //
    }
}

class InvalidParam1{
    /**
     * @o-route GET /
     * @param $param2
     */
    public function test($param0, $param1=1){
        //
    }
}

class InvalidParam2{
    /**
     * @o-route GET /
     * @param 123 $param0
     */
    public function test($param0, $param1=1){
        //
    }
}

/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/8
 * Time: 下午2:26
 */
class AnnParamTest extends TestCase
{
    public function testValidParam(){
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, ValidParam::class);

        $route = $container->getRoute('funWithoutParams0');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(2, count($params));


        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>null,
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[0]));
        $this->assertEquals(get_object_vars($params[1]), [
            'name'=>'param1',
            'source'=>'$.request.query.param1',
            'type'=>null,
            'default'=>1,
            'isOptional'=>true,
            'doc'=>'',
            'isPassedByReference'=>false,
            'validation'=>null
        ]);

        $route = $container->getRoute('funWithoutParams1');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(2, count($params));

        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>'string',
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[0]));

        $this->assertEquals([
            'name'=>'param1',
            'source'=>'$.request.query.param1',
            'type'=>null,
            'default'=>1,
            'isOptional'=>true,
            'doc'=>'',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[1]));


        $route = $container->getRoute('funWithoutParams2');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(1, count($params));

        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>'string',
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'doc',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[0]));

        $route = $container->getRoute('funWithoutParams3');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(1, count($params));

        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>null,
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'doc',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[0]));

        $route = $container->getRoute('funWithoutParams4');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(1, count($params));

        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>null,
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[0]));

        $route = $container->getRoute('funWithoutParams5');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(1, count($params));

        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>null,
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'',
            'isPassedByReference'=>true,
            'validation'=>null],
            get_object_vars($params[0]));

        $route = $container->getRoute('funWithoutParams6');
        $params = $route->getActionInvoker()->getParamsBuilder()->getParams();
        $this->assertEquals(1, count($params));

        $this->assertEquals([
            'name'=>'param0',
            'source'=>'$.request.query.param0',
            'type'=>'\Once\Annotations\Controller\AnnParam',
            'default'=>null,
            'isOptional'=>false,
            'doc'=>'',
            'isPassedByReference'=>false,
            'validation'=>null],
            get_object_vars($params[0]));
    }

    public function testInvalidParam0()
    {
        $this->expectException(\Once\Exceptions\AnnotationSyntaxExceptions::class);
        $this->expectExceptionMessage('"@param [type] <param>" miss params for InvalidParam0::test');
        $container = \Once\Api::getInstance()->loadRoutesFromClass($this->app, InvalidParam0::class);
    }
    public function testInvalidParam1()
    {
        $this->expectException(\Once\Exceptions\AnnotationSyntaxExceptions::class);
        $this->expectExceptionMessage('InvalidParam1::test param param2 not exist');
        $container = \Once\Api::getInstance()->loadRoutesFromClass($this->app, InvalidParam1::class);
    }
//    public function testInvalidParam2()
//    {
//        $this->expectException(\Once\Exceptions\AnnotationSyntaxExceptions::class);
//        $this->expectExceptionMessage('InvalidParam1::test param param2 not exist');
//        $container = \Once\Api::loadRoutesFromClass($this->app, InvalidParam2::class);
//    }
}
