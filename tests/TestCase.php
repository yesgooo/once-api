<?php
ini_set('date.timezone','Asia/Shanghai');
require __DIR__.'/../vendor/autoload.php';

class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $app = new Laravel\Lumen\Application(
            realpath(__DIR__ . '/../')
        );
        $app->withFacades();
        return $app;
    }
}
