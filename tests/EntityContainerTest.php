<?php
require __DIR__.'/../vendor/autoload.php';

class TestProperty{
    public $property0;
}
/**
 * title
 * Class TestPropertyAnn
 */
class TestPropertyAnn{

    public $property0;

    /**
     * property1
     */
    public $property1;

    /**
     * @var string
     */
    public $property2;

    /**
     * @var integer
     * @o-vld min:10|max:20
     */
    public $property3;

    /**
     * @var TestProperty
     */
    public $property4;

    /**
     * @var
     * @o-vld
     */
    public $property5;

    /**
     * @var string
     */
    public $propertyWithDefault="default";

}


class TestPropertyTypeInt{
    /**
     * @var integer
     */
    public $property0;
}

class TestPropertyTypeString{
    /**
     * @var string
     */
    public $property0;
}

class TestPropertyTypeArray{
    /**
     * @var string[]
     */
    public $property0;
}

class TestPropertyTypeObject{
    /**
     * @var TestProperty
     */
    public $property0;
}

class TestPropertyTypeObjectArray{
    /**
     * @var TestProperty[]
     */
    public $property0;
}


class EntityWithInvalidAnnVar{
    /**
     * @var UnknownType
     */
    public $property0;
}

class EntityWithInvalidAnnVlt{
    /**
     * @o-vld unknown
     */
    public $property0;
}
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/10
 * Time: 下午9:35
 */
class EntityContainerTest extends TestCase
{
    public function testMeta(){
        $container  = \Once\Api::getInstance()->getEntity(TestPropertyAnn::class);
        $properties = $container->getProperties();
        $this->assertEquals(count($properties), 7);

        $this->assertEquals($container->getDoc(), "title\nClass TestPropertyAnn");
        //测试无注释
        $this->assertEquals(get_object_vars($properties['property0']),
            [
                'name'=>'property0',
                'type'=>null,
                'default'=>null,
                'isOptional'=>false,
                'validation'=>null,
                'doc'=>'',
            ]);
        // 测试普通注释
        $this->assertEquals(get_object_vars($properties['property1']),
            [
                'name'=>'property1',
                'type'=>null,
                'default'=>null,
                'isOptional'=>false,
                'validation'=>null,
                'doc'=>'property1',
            ]);
        //测试 @var
        $this->assertEquals(get_object_vars($properties['property2']),
            [
                'name'=>'property2',
                'type'=>'string',
                'default'=>null,
                'isOptional'=>false,
                'validation'=>null,
                'doc'=>'',
            ]);
        //测试validation
        $this->assertEquals(get_object_vars($properties['property3']),
            [
                'name'=>'property3',
                'type'=>'int',
                'default'=>null,
                'isOptional'=>false,
                'validation'=>'min:10|max:20',
                'doc'=>'',
            ]);
        //测试对象类型属性
        $this->assertEquals(get_object_vars($properties['property4']),
            [
                'name'=>'property4',
                'type'=>'\TestProperty',
                'default'=>null,
                'isOptional'=>false,
                'validation'=>null,
                'doc'=>'',
            ]);

        //测试@var @vld 为空
        $this->assertEquals(get_object_vars($properties['property5']),
            [
                'name'=>'property5',
                'type'=>null,
                'default'=>null,
                'isOptional'=>false,
                'validation'=>null,
                'doc'=>'',
            ]);

        //测试默认值
        $this->assertEquals(get_object_vars($properties['propertyWithDefault']),
            [
                'name'=>'propertyWithDefault',
                'type'=>'string',
                'default'=>'default',
                'isOptional'=>true,
                'validation'=>null,
                'doc'=>'',
            ]);

    }
    public function testIvalidAnnVar(){
        //TODO
//        $this->expectException(
//            new \Once\Exceptions\AnnotationSyntaxExceptions(
//                'unknown type "UnknownType" for property TestPropertyAnn::property0'
//            )
//        );
//        $container  = \Once\Api::getEntity(TestPropertyAnn::class);
    }

    public function testIvalidAnnVld(){
        //TODO
//        $this->expectException(
//            new \Once\Exceptions\AnnotationSyntaxExceptions(
//                'invalid validation "unknown" for property TestPropertyAnn::property0'
//            )
//        );
//        $container  = \Once\Api::getEntity(TestPropertyAnn::class);
    }

    public function testMultiPropertiesMake(){
        $params = [
            'property0'=>'property0',
            'property1'=>'property1',
            'property2'=>'property2',
            'property3'=>12,
            'property4'=>['property0'=>'property4'],
            'property5'=>'property5',
            'propertyWithDefault'=>'propertyWithDefault'
        ];
        $params2 = $params;
        $params2['property4'] = new TestProperty();
        $params2['property4']->property0 = 'property4';
        $this->assertEquals(get_object_vars(\Once\Api::getInstance()->makeEntity(
            $this->app,
            TestPropertyAnn::class,
            $params
            )),$params2);
    }

    public function testIntPropertyMake(){
        //int -> int
        $params = [
            'property0'=>123,
        ];
        $this->assertSame(\Once\Api::getInstance()->makeEntity(
            $this->app,
            TestPropertyTypeInt::class,
            $params
        )->property0, 123);

        //string -> int
        $params = [
            'property0'=>"123",
        ];
        $this->assertSame(\Once\Api::getInstance()->getInstance()->makeEntity(
            $this->app,
            TestPropertyTypeInt::class,
            $params
        )->property0, '123');



    }

    //TODO 测试校验失败
    //TODO 测试缺少必选项
    //TODO 测试默认值
}