<?php
require __DIR__.'/../vendor/autoload.php';

/**
 * Class ValidRoute
 */
class ValidRoute{
    /**
     * testGet
     * testGet doc
     * @o-route GET /testGet
     */
    public function testGet(){
        return __FUNCTION__;
    }

    /**
     * @o-route GET /testGetWithPathArg/{arg1}
     */
    public function testGetWithPathArg(){
        return __FUNCTION__;
    }

    /**
     * testHead
     * testHead doc
     * @o-route HEAD /testHead/
     */
    public function testHead(){
        return __FUNCTION__;
    }

    /**
     * @o-route PUT /testPut/
     */
    public function testPut(){
        return __FUNCTION__;
    }

    /**
     * @o-route POST /testPost/
     */
    public function testPost(){
        return __FUNCTION__;
    }

    /**
     * @o-route DELETE /testDelete/
     */
    public function testDelete(){
        return __FUNCTION__;
    }

    /**
     * @o-route OPTIONS /testOptions/
     */
    public function testOptions(){
        return __FUNCTION__;
    }

    /**
     * @o-route PATCH /testPatch/
     */
    public function testPatch(){
        return __FUNCTION__;
    }


}

class InvalidRouteWithBadMethod{
    /**
     * @o-route UNKNOWN /
     */
    public function test(){

    }
}
class InvalidRouteWithParams1{
    /**
     * @o-route UNKNOWN
     */
    public function test(){

    }
}
class InvalidRouteWithParams2{
    /**
     * @o-route
     */
    public function test(){

    }
}
// TODO: * 测试route冲突
// TODO: 测试继承

/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/9
 * Time: 下午4:19
 */
class AnnRouteTest extends TestCase
{

    public function testRouteDoc(){
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, ValidRoute::class);
        $route = $container->getRoute('testGet');

        $this->assertEquals($route->getDoc(), "testGet\ntestGet doc");
    }

    public function testRouteInvalidMethod(){
        $this->expectException(\Once\Exceptions\AnnotationSyntaxExceptions::class);
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, InvalidRouteWithBadMethod::class);
    }
    public function testRouteInvalidParams1(){
        $this->expectException(\Once\Exceptions\AnnotationSyntaxExceptions::class);
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, InvalidRouteWithParams1::class);
    }
    public function testRouteInvalidParams2(){
        $this->expectException(\Once\Exceptions\AnnotationSyntaxExceptions::class);
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, InvalidRouteWithParams2::class);
    }

    public function testValidRoute(){
        $container  = \Once\Api::getInstance()->loadRoutesFromClass($this->app, ValidRoute::class);
        $routes = $container->getRoutes();

        $this->assertEquals(count($routes), 8);

        $this->get('/testGet');
        $this->assertEquals(
           'testGet', $this->response->getContent()
        );

        $this->get('/testGetWithPathArg/1');
        $this->assertEquals(
            'testGetWithPathArg', $this->response->getContent()
        );

        $this->post('/testPost');
        $this->assertEquals(
            'testPost', $this->response->getContent()
        );


//TODO: 测试head请求
//        $this->head('/testHead');
//        $this->assertEquals(
//            'testHead', $this->response->getContent()
//        );

        $this->delete('/testDelete');
        $this->assertEquals(
            'testDelete', $this->response->getContent()
        );

        $this->put('/testPut');
        $this->assertEquals(
            'testPut', $this->response->getContent()
        );

//TODO: 测试options请求
//        $this->options('/testOptions');
//        $this->assertEquals(
//            'testOptions', $this->response->getContent()
//        );

        $this->patch('/testPatch');
        $this->assertEquals(
            'testPatch', $this->response->getContent()
        );


    }
    //TODO 无效的@path检查
}