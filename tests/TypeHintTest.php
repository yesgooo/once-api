<?php
require __DIR__.'/../vendor/autoload.php';

use \fixtures\TypeHintClassy;
/**
 * Created by PhpStorm.
 * User: caoyangmin
 * Date: 16/11/11
 * Time: 下午10:24
 */
class TypeHintTest  extends TestCase
{
    /**
     * @param int $i
     */
    public function test(){
        $this->assertSame(
            \Once\Utils\TypeHint::normalize('integer', __CLASS__),
            'int');

        $this->assertSame(
            \Once\Utils\TypeHint::normalize('integer[]', __CLASS__),
            'int[]');

        $this->assertSame(
            \Once\Utils\TypeHint::normalize('array', __CLASS__),
            'array');


        $this->assertSame(
            \Once\Utils\TypeHint::normalize('TypeHintClassy', __CLASS__),
            '\fixtures\TypeHintClassy');


    }
}